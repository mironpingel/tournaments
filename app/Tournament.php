<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = [
        "game_id",
        "name",
    ];

    protected $with = [
        "players"
    ];

    public function path()
    {
       return "api/tournaments/{$this->id}";
    }

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function characters()
    {
        return $this->belongsToMany(Character::class, 'tournament_characters');
    }

    public function played_characters()
    {
        return $this->belongsToMany(Character::class, 'characters_played', 'character_id', 'tournament_id')->withPivot('player_id');
    }

    public function players()
    {
        return $this->belongsToMany(Player::class, 'tournament_players')->withPivot('score');
    }

    public function modifyPlayerScoreBy(int $amount, Player $player)
    {
        $currentScore = $this->players()->find($player->id)->pivot->score;

        $this->players()->updateExistingPivot($player->id, ["score" => $currentScore + $amount]);

        return $player;
    }

    public function addPlayer(Player $player, array $attributes = [])
    {
        return $this->players()->attach($player, $attributes);
    }

    public function removePlayer(Player $player)
    {
        return $this->players()->detach($player);
    }

    public function addCharacter(Character $character, array $attributes = [])
    {
        return $this->characters()->attach($character, $attributes);
    }

    public function removeCharacter(Character $character)
    {
        return $this->characters()->detach($character);
    }

    public function playerPlayed(Character $character, Player $player)
    {
        if ($this->played_characters()->get()->contains($character)) {
            return false;
        }

        return $this->played_characters()->attach($character, [ "player_id" => $player->id ]);
    }

    public function playerDidNotPlay(Character $character, Player $player)
    {
        if (!$this->played_characters()->get()->contains($character)) {
            return false;
        }

        return $this->played_characters()->wherePivot('player_id', "=", $player->id)->detach($character);
    }
}
