<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        "name"
    ];

    public function tournaments()
    {
        return $this->hasMany(Tournament::class);
    }
}
