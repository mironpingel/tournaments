<?php

namespace App\Http\Controllers;

use App\Character;
use App\Tournament;
use Illuminate\Http\Request;

class TournamentCharacterController extends Controller
{

    public function index(Tournament $tournament)
    {
        return $tournament->characters->sortBy('name');
    }

    public function attach(Tournament $tournament)
    {
        $character = Character::find(request('character_id'));

        $tournament->addCharacter($character);

        return response('Character was added to the Tournament', 200);
    }

    public function detach(Tournament $tournament, Character $character)
    {
        $tournament->removeCharacter($character);

        return response('Character has been removed from the Tournament', 200);
    }
}
