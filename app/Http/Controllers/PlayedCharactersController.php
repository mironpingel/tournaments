<?php

namespace App\Http\Controllers;

use App\Character;
use App\Player;
use App\Tournament;
use Illuminate\Http\Request;

class PlayedCharactersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tournament $tournament)
    {
        return $tournament->played_characters;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tournament $tournament, Character $character)
    {
        $player = Player::find(request('player_id'));
        $tournament->playerDidNotPlay($character, $player);

        return $tournament->played_characters()->get();
    }
}
