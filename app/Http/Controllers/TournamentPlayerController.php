<?php

namespace App\Http\Controllers;

use App\Player;
use App\Tournament;
use Illuminate\Http\Request;

class TournamentPlayerController extends Controller
{
    public function attach(Tournament $tournament)
    {
       $player = Player::find(request('player_id'));

       $tournament->addPlayer($player);

        return response('Player was added to the Tournament', 200);
    }

    public function detach(Tournament $tournament, Player $player)
    {
        $tournament->removePlayer($player);

        return response('Player has been removed from the Tournament', 200);
    }
}
