<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $fillable = [
        "name",
        "image"
    ];

    public function tournaments()
    {
        return $this->belongsToMany(Tournament::class, 'tournament_characters');
    }
}
