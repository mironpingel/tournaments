<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
        "name"
    ];

    public function tournaments()
    {
        return $this->belongsToMany(Tournament::class, 'tournament_players')->withPivot('score');
    }
}
