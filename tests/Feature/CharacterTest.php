<?php

namespace Tests\Feature;

use App\Character;
use App\Player;
use App\Tournament;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CharacterTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * @test
     */
    public function a_character_can_be_created()
    {
        $attributes = [
            "name" => $this->faker->name
        ];

        $this->post('api/characters', $attributes)->assertSee($attributes['name']);

        $this->assertDatabaseHas('characters', $attributes);
    }

    /**
     * @test
     */
    public function a_character_can_be_deleted()
    {
       $character = factory(Character::class)->create();

       $this->delete("api/characters/{$character->id}")->assertStatus(204);

       $this->assertDatabaseMissing('characters', $character->toArray());
    }

    /**
     * @test
     */
    public function a_character_can_be_played_by_a_player()
    {
        $character = factory(Character::class)->create();
        $player = factory(Player::class)->create();
        $tournament = factory(Tournament::class)->create();

        $tournament->addPlayer($player);
        $tournament->addCharacter($character);

        // Should be refactored to:
        // $tournament->player($player)->played($character);

        $tournament->playerPlayed($character, $player);

        $this->assertDatabaseHas('characters_played', [
            "character_id" => $character->id,
            "player_id" => $player->id,
            "tournament_id" => $tournament->id
        ]);
    }

    /**
     * @test
     */
    public function a_played_character_can_be_removed()
    {
        $character = factory(Character::class)->create();
        $player = factory(Player::class)->create();
        $tournament = factory(Tournament::class)->create();

        $tournament->addPlayer($player);
        $tournament->addCharacter($character);
        $tournament->playerPlayed($character, $player);

        // Make sure its added
        $this->assertDatabaseHas('characters_played', [ "character_id" => $character->id, "player_id" => $player->id, "tournament_id" => $tournament->id ]);

        // Remove it
        $tournament->playerNotPlayed($character, $player);

        // Make sure its gone
        $this->assertDatabaseMissing('characters_played', [ "character_id" => $character->id, "player_id" => $player->id, "tournament_id" => $tournament->id ]);
    }


    /**
     * @test
     */
    public function played_characters_can_be_fetched()
    {
        $character = factory(Character::class)->create();
        $player = factory(Player::class)->create();
        $tournament = factory(Tournament::class)->create();

        $tournament->addPlayer($player);
        $tournament->addCharacter($character);

        $tournament->playerPlayed($character, $player);

        $this->get('/api/tournaments/' . $tournament->id . '/characters/played')->assertSee($character->name);
    }

    /**
     * @test
     */
    public function a_played_character_can_be_deleted()
    {
        $this->withoutExceptionHandling();
        $character = factory(Character::class)->create();
        $player = factory(Player::class)->create();
        $tournament = factory(Tournament::class)->create();

        $tournament->addPlayer($player);
        $tournament->addCharacter($character);

        $tournament->playerPlayed($character, $player);

        $this->delete("/api/tournaments/{$tournament->id}/characters/played/{$character->id}?player_id={$player->id}")->assertStatus(200);
        $this->assertDatabaseMissing('characters_played', [ "character_id" => $character->id, "player_id" => $player->id, "tournament_id" => $tournament->id ]);
    }
}
