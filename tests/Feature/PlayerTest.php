<?php

namespace Tests\Feature;

use App\Player;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlayerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * @test
     */
    public function a_player_can_be_created()
    {
        $attributes = [
            "name" => $this->faker->name
        ];

        $this->post('api/players', $attributes)->assertSee($attributes['name']);

        $this->assertDatabaseHas('players', $attributes);
    }

    /**
     * @test
     */
    public function a_player_can_be_deleted()
    {
       $player = factory(Player::class)->create();

       $this->delete("api/players/{$player->id}")->assertStatus(204);

       $this->assertDatabaseMissing('players', $player->toArray());
    }
}
