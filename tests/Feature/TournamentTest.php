<?php

namespace Tests\Feature;

use App\Character;
use App\Game;
use App\Player;
use App\Tournament;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TournamentTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * @test
     */
    public function a_user_can_create_a_tournament()
    {
        $attributes = [
            "game_id" => 1,
            "name" => $this->faker->name
        ];

        $this->post('api/tournaments', $attributes);

        $this->assertDatabaseHas('tournaments', $attributes);
    }

    /**
     * @test
     */
    public function a_tournament_can_be_fetched()
    {
        $attributes = [
            "game_id" => 1,
            "name" => $this->faker->name
        ];

        $tournament = factory(Tournament::class)->create($attributes);

        $this->get($tournament->path())->assertSee($tournament->name);
    }

    /**
     * @test
     */
    public function a_set_of_all_tournaments_can_be_fetched()
    {
        $tournaments = factory(Tournament::class, 3)->create();

        // Make sure all three created tournaments are displayed
        $this->get('api/tournaments')->assertSee($tournaments[0]->name)
                                        ->assertSee($tournaments[1]->name)
                                        ->assertSee($tournaments[2]->name);
    }

    /**
     * @test
     */
    public function a_tournament_knows_which_game_it_belongs_to()
    {
        $game = factory(Game::class)->create([
            "id" => 1,
            "name" => $this->faker->name
        ]);

        $tournament = factory(Tournament::class)->create([
            "game_id" => $game->id,
            "name" => $this->faker->name
        ]);

        $this->assertEquals($game->name, $tournament->game->name);
    }

    /**
     * @test
     */
    public function it_can_have_characters_associated_with_it()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $character = factory(Character::class)->create([
            "name" => $this->faker->name
        ]);

        $tournament->characters()->attach($character);

        $this->assertEquals($tournament->characters->first()->name, $character->name);
    }

    /**
     * @test
     */
    public function it_can_have_players_associated_with_it()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $player = factory(Player::class)->create([
            "name" => $this->faker->name
        ]);

        $tournament->addPlayer($player);

        $this->assertEquals($tournament->players->first()->name, $player->name);
    }

    /**
     * @test
     */
    public function it_includes_the_players_when_fetched()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $player = factory(Player::class)->create([
            "name" => $this->faker->name
        ]);

        $tournament->addPlayer($player);

        $this->get($tournament->path())->assertSee($player->name);
    }

    /**
     * @test
     */
    public function a_new_tournament_can_be_created()
    {
        $game = factory(Game::class)->create();

        $attributes = [
            "name" => $this->faker->name,
            "game_id" => $game->id,
        ];

        $this->post('/api/tournaments', $attributes)->assertSee($attributes["name"]); // Make sure the tournament is returned

        $this->assertDatabaseHas('tournaments', $attributes); // Make sure it is persisted
    }

    /**
     * @test
     */
    public function players_in_the_tournament_have_a_score()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $player = factory(Player::class)->create([
            "name" => $this->faker->name
        ]);

        $tournament->addPlayer($player, ["score" => 5]);

        $this->assertEquals($tournament->players->first()->pivot->score, 5);
    }

    /**
     * @test
     */
    public function a_users_score_can_be_updated()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $player = factory(Player::class)->create([
            "name" => $this->faker->name
        ]);

        $tournament->addPlayer($player, ["score" => 5]); // 5

        // Add to Score
        $tournament->modifyPlayerScoreBy(5, $player);
        $this->assertEquals($tournament->players()->find($player->id)->pivot->score, 10);

        // Subtract from Score
        $tournament->modifyPlayerScoreBy(-2, $player);
        $this->assertEquals($tournament->players()->find($player->id)->pivot->score, 8);
    }

    /**
     * @test
     */
    public function a_player_can_be_added_to_the_tournament()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $player = factory(Player::class)->create([
            "name" => $this->faker->name
        ]);

        $this->post("api/tournaments/{$tournament->id}/players/attach", ["player_id" => $player->id])
             ->assertStatus(200);

        $this->assertDatabaseHas('tournament_players', [
            "tournament_id" => $tournament->id,
            "player_id" => $player->id
        ]);
    }

    /**
     * @test
     */
    public function a_player_can_be_removed_from_the_tournament()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $player = factory(Player::class)->create([
            "name" => $this->faker->name
        ]);

        $this->post("api/tournaments/{$tournament->id}/players/{$player->id}/detach")
            ->assertStatus(200);

        $this->assertDatabaseMissing('tournament_players', [
            "tournament_id" => $tournament->id,
            "player_id" => $player->id
        ]);
    }

    /**
     * @test
     */
    public function a_character_can_be_added_to_the_tournament()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $character = factory(Character::class)->create([
            "name" => $this->faker->name
        ]);

        $this->post("api/tournaments/{$tournament->id}/characters/attach", ["character_id" => $character->id])
            ->assertStatus(200);

        $this->assertDatabaseHas('tournament_characters', [
            "tournament_id" => $tournament->id,
            "character_id" => $character->id
        ]);
    }

    /**
     * @test
     */
    public function a_character_can_be_removed_from_the_tournament()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $character = factory(Character::class)->create([
            "name" => $this->faker->name
        ]);

        $this->post("api/tournaments/{$tournament->id}/characters/{$character->id}/detach")
            ->assertStatus(200);

        $this->assertDatabaseMissing('tournament_characters', [
            "tournament_id" => $tournament->id,
            "character_id" => $character->id
        ]);
    }

    /**
     * @test
     */
    public function all_characters_for_a_given_tournament_can_be_retrieved()
    {
        $tournament = factory(Tournament::class)->create([
            "game_id" => 1,
            "name" => $this->faker->name
        ]);

        $character = factory(Character::class)->create([
            "name" => $this->faker->name
        ]);

        $tournament->characters()->attach($character);

        $this->get("api/tournaments/{$tournament->id}/characters")
            ->assertStatus(200)
            ->assertSee($character->name);
    }
}
