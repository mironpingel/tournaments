<?php

namespace Tests\Feature;

use App\Game;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GameTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * @test
     */
    public function a_game_can_be_created()
    {
        $attributes = [
            "name" => $this->faker->name
        ];

        $this->post('api/games', $attributes)->assertSee($attributes['name']);

        $this->assertDatabaseHas('games', $attributes);
    }

    /**
     * @test
     */
    public function a_game_can_be_deleted()
    {
       $game = factory(Game::class)->create();

       $this->delete("api/games/{$game->id}")->assertStatus(204);

       $this->assertDatabaseMissing('games', $game->toArray());
    }
}
