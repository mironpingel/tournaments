<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('tournaments', 'TournamentController');

// Tournament - Players
Route::post('tournaments/{tournament}/players/attach', 'TournamentPlayerController@attach');
Route::post('tournaments/{tournament}/players/{player}/detach', 'TournamentPlayerController@detach');


// Tournament - Characters
Route::get('tournaments/{tournament}/characters', 'TournamentCharacterController@index');
Route::get('tournaments/{tournament}/characters/played', 'PlayedCharactersController@index');
Route::delete('tournaments/{tournament}/characters/played/{character}', 'PlayedCharactersController@destroy');
Route::post('tournaments/{tournament}/characters/attach', 'TournamentCharacterController@attach');
Route::post('tournaments/{tournament}/characters/{character}/detach', 'TournamentCharacterController@detach');



Route::apiResource('players', 'PlayerController');

Route::apiResource('characters', 'CharacterController');

Route::apiResource('games', 'GameController');
