import axios from 'axios';
import moxios from 'moxios';
import {mount} from '@vue/test-utils';
import TournamentTopBar from '@/components/TournamentTopBar.vue';
import mockData from '../mockdata';
import {see, interceptAxiosResponseWith} from "../helpers";

describe('TournamentTopBar.vue', () => {
    // beforeEach(function () {
    //     // import and pass your custom axios instance to this method
    //     moxios.install()
    // });
    //
    // afterEach(function () {
    //     // import and pass your custom axios instance to this method
    //     moxios.uninstall()
    // });


    it('Fetches and Renders name of Tournament', () => {
        const wrapper = mount(TournamentTopBar);

        see('hello', wrapper);
    });

});
