import axios from 'axios';
import moxios from 'moxios';
import { mount } from '@vue/test-utils';
import TournamentIndex from '@/views/TournamentIndex.vue';
import mockData from '../mockdata';
import {see, interceptAxiosResponseWith} from "../helpers";


describe('TournamentIndex.vue', () => {
    beforeEach(function () {
        moxios.install()
    });

    afterEach(function () {
        moxios.uninstall()
    });


    it('Fetches and Renders all Tournaments', (done) => {
        const wrapper = mount(TournamentIndex);

        moxios.wait(async () => {
           await interceptAxiosResponseWith(mockData.setOfTournaments);

            see(mockData.setOfTournaments[0].name, wrapper);
            see(mockData.setOfTournaments[1].name, wrapper);

            done();
        });
    });

    it('should display the players for each Tournament', (done) => {
        const wrapper = mount(TournamentIndex);

        moxios.wait(async () => {
            await interceptAxiosResponseWith(mockData.setOfTournaments);

            see(mockData.setOfTournaments[0].players[0].name, wrapper);
            see(mockData.setOfTournaments[1].players[1].name, wrapper);

            done();
        })
    });

    it('should display the score for each player in the tournament', (done) => {
        const wrapper = mount(TournamentIndex);

        moxios.wait(async () => {
            await interceptAxiosResponseWith(mockData.setOfTournaments);

            see(mockData.setOfTournaments[0].players[0].score, wrapper, ".players");
            see(mockData.setOfTournaments[1].players[1].score, wrapper, '.players');

            done();
        });
    });

});
