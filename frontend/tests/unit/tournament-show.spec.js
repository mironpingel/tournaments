import axios from 'axios';
import moxios from 'moxios';
import {mount} from '@vue/test-utils';
import TournamentShow from '@/views/TournamentShow.vue';
import mockData from '../mockdata';
import {see, interceptAxiosResponseWith} from "../helpers";

describe('TournamentShow.vue', () => {
    beforeEach(function () {
        // import and pass your custom axios instance to this method
        moxios.install()
    });

    afterEach(function () {
        // import and pass your custom axios instance to this method
        moxios.uninstall()
    });


    it('Fetches and Renders name of Tournament', (done) => {
        const $route = {params: {id: 1}};
        const wrapper = mount(TournamentShow, {mocks: {$route}});

        moxios.wait(async () => {
            await interceptAxiosResponseWith(mockData.singleTournament);

            see(mockData.singleTournament.name, wrapper);

            done();
        });
    });

    // it('should render the names of all the characters it contains', (done) => {
    //     const $route = {params: {id: 1}};
    //     const wrapper = mount(TournamentShow, {mocks: {$route}});
    //
    //     moxios.wait(async () => {
    //         await interceptAxiosResponseWith(mockData.singleTournament);
    //
    //         mockData.singleTournament.characters.forEach(character => {
    //             see(character.name, wrapper, '.characters');
    //         });
    //
    //         done();
    //     });
    // });

    it('should display the names and scores of each player in the tournament', (done) => {
        const $route = {params: {id: 1}};
        const wrapper = mount(TournamentShow, {mocks: {$route}});

        moxios.wait(async () => {
            await interceptAxiosResponseWith(mockData.singleTournament);

            mockData.singleTournament.players.forEach(player => {
                see(player.name, wrapper, '.players');
                see(player.score, wrapper, '.players');
            });

            done();
        });
    });

});

