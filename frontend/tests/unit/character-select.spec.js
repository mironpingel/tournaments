import axios from 'axios';
import moxios from 'moxios';
import {mount} from '@vue/test-utils';
import CharacterSelect from '@/components/CharacterSelect.vue';
import mockData from '../mockdata';
import {see, interceptAxiosResponseWith} from "../helpers";

describe('CharacterSelect.vue', () => {
    beforeEach(function () {
        // import and pass your custom axios instance to this method
        moxios.install()
    });

    afterEach(function () {
        // import and pass your custom axios instance to this method
        moxios.uninstall()
    });

    it('should render the names of all the characters contained in the tournament', (done) => {
        const wrapper = mount(CharacterSelect, {
            propsData: { tournament: mockData.singleTournament }
        });

        moxios.wait(async () => {
            await interceptAxiosResponseWith(mockData.characters);

            mockData.characters.forEach(character => {
                see(character.name, wrapper, '.characters');
            });

            done();
        });
    });
});
