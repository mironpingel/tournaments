export default {
    singleTournament: {
        id: 1,
        game_id: 1,
        name: "Super Smash Ultimate",
        players: [
            {
                name: "Miron",
                score: 42
            },
            {
                name: "Kim Joschua",
                score: 30
            }
        ]
    },

    setOfTournaments: [
        {
            id: 1,
            game_id: 1,
            name: "Super Smash Ultimate",
            players: [
                {
                    name: "Miron",
                    score: 42
                },
                {
                    name: "Kim Joschua",
                    score: 30
                }
            ]
        },
        {
            id: 2,
            game_id: 1,
            name: "Dragonball Xenoverse",
            players: [
                {
                    name: "Miron",
                    score: 42
                },
                {
                    name: "Kim Joschua",
                    score: 30
                }
            ]
        }
    ],
    characters: [
        {
            name: 'Mario'
        },
        {
            name: 'Wolf'
        },
    ],
}