import moxios from 'moxios';

export async function interceptAxiosResponseWith (response, status = 200) {
    const request = moxios.requests.mostRecent();
    const mockResponse = {
        status,
        response
    };

    await request.respondWith(mockResponse);
};

export function see (text, currentWrapper, selector) {
    let componentWrapper = selector ? currentWrapper.find(selector) : currentWrapper;

    expect(componentWrapper.html()).toContain(text);
};

