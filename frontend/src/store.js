import Vue from 'vue';
import Vuex from 'vuex';
import Characters from './stores/characters'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ...Characters.state
  },

  mutations: {
    ...Characters.mutations
  },

  actions: {
    ...Characters.actions
  },
});
