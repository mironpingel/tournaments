import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import TournamentIndex from './views/TournamentIndex';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: TournamentIndex,
    },
    {
      path: '/tournaments/:id',
      name: 'tournament-show',
      component: () => import(/* webpackChunkName: "tournament-show" */ './views/TournamentShow.vue'),
    },
  ],
});
