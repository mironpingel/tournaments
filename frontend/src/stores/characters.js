import axios from 'axios';

export default{
    state: {
        characters: [],
        playedCharacters: []
    },

    mutations: {
        setCharacters(state, characters) {
            state.characters = characters;
        },

        setPlayedCharacters(state, playedCharacters) {
           state.playedCharacters = playedCharacters;
        },
    },

    actions: {
        async fetchCharacters(context, tournament_id) {
            const { data } = await axios.get(`/api/tournaments/${tournament_id}/characters`);

            context.commit('setCharacters', data);
        },

        async fetchPlayedCharacters(context, tournament_id) {
            const { data } = await axios.get(`/api/tournaments/${tournament_id}/characters/played`);

            context.commit('setPlayedCharacters', data);
        },

        async removePlayedCharacter(context, character) {
            const { data } = await axios.delete(`/api/tournaments/${character.pivot.tournament_id}/characters/played/${character.id}?player_id=${character.pivot.player_id}`);

            // Set Updated collection of played characters
            context.commit('setPlayedCharacters', data);
        },
    },
};
